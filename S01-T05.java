import java.util.Scanner;

public class S01-T05{
    public static void main(String[] args) {
        try(Scanner scan = new Scanner(System.in)){
            System.out.println("Введите три целых числа: ");
            int x = scan.nextInt();             
            int y = scan.nextInt(); 
            int z = scan.nextInt(); 
            if(x * x + y * y == z * z || z * z + y * y == x * x || z * z + x * x == y * y){
                System.out.println("Являются");
            }
            else{
                System.out.println(" Не являются");
            }    
        } catch(Exception e) {
            e.printStackTrace();
        }    
    }
}    
