
public class n11sr1 {
    public static void main(String[] args) {
        int[] osadki = new int[]{23, 34, 45, 45, 34, 23, 2, 43, 57, 72, 71, 2, 34, 34, 54, 34, 64, 47, 43, 62, 36, 66, 75, 54, 34, 32, 23, 54, 65, 99};
        int min;
        int max;
        int daymin = 0;
        int daymax = 0;
        min = max = osadki[0];
        for(int i = 0; i < 30; i++){
            if(osadki[i] < min){
                min = osadki[i];
                daymin = i;
            }
            if(osadki[i] > max){
                max = osadki[i];
                daymax = i;
            }
        }
        System.out.println("Максимальное количество осадков " + max + " было " + daymax + " день месяца");
        System.out.println("Минимальное количество осадков " + min + " было " + daymin + " день месяца");
    }
}