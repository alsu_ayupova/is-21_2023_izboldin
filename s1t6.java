import java.util.Scanner;

public class n6sr1 {
    public static void main(String[] args) {
        try(Scanner scan = new Scanner(System.in)){
            System.out.println("Введите  радиус:");
            double r = scan.nextDouble();
            System.out.println("Площадь: " + Math.PI * r * r);
            System.out.println("Длина: " + 2 * Math.PI * r);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }        
}