import java.util.Scanner;

public class n9sr1 {
    public static void main(String[] args) {
        try(Scanner scan = new Scanner(System.in)){
            System.out.println("Введите число: ");
            int n = scan.nextInt();
            int sum = 0;
            while(n != 0){
                sum = sum + (n % 10);
                n = n / 10;
            }
            System.out.println(sum);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }    
}