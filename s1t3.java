import java.util.Scanner;

public class n3sr1 {
    public static void main(String[] args) {
        try(Scanner scan = new Scanner(System.in)){
            System.out.println("Введите двузначное число: ");
            int a = scan.nextInt();
            System.out.println(a / 10);
            System.out.println(a % 10);
            System.out.println((a  / 10) + (a % 10));
            System.out.println((a  / 10) * (a % 10));  
        } catch(Exception e) {
            e.printStackTrace();
        }
    }     
}

