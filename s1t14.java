import java.util.Scanner;

public class n14sr1 {
    public static void main(String[] args) {
        try(Scanner scan = new Scanner(System.in)){
            System.out.println("Введите N: ");
            int n = scan.nextInt();
            int a = 0;
            for(int i = 1; i <= n; i++){
               a += i * i;
            }
            System.out.println(a);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }    
}